package com.travelcar.data.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.travelcar.data.common.database.travelcar.Converters
import com.travelcar.data.common.database.travelcar.TravelCarDao
import com.travelcar.data.common.database.travelcar.TravelCarEntity

@Database(entities = [TravelCarEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class TravelCarDatabase : RoomDatabase() {
    abstract fun travelCarDao(): TravelCarDao
}

internal const val TRAVEL_CAR_DATABASE_NAME = "travel_car_database"