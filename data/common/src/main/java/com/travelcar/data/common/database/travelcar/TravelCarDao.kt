package com.travelcar.data.common.database.travelcar

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.travelcar.data.common.database.travelcar.TravelCarEntity.Companion.TRAVEL_CAR_TABLE_NAME
import kotlinx.coroutines.flow.Flow

@Dao
interface TravelCarDao {

    @Insert
    suspend fun insertCars(cars: List<TravelCarEntity>)

    @Query("SELECT * FROM $TRAVEL_CAR_TABLE_NAME")
    suspend fun getAllCars(): List<TravelCarEntity>?

    @Query("SELECT * FROM $TRAVEL_CAR_TABLE_NAME")
    fun observeAllCars(): Flow<List<TravelCarEntity>?>
}
