package com.travelcar.data.common.database.travelcar

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.travelcar.data.common.database.travelcar.TravelCarEntity.Companion.TRAVEL_CAR_TABLE_NAME

@Entity(tableName = TRAVEL_CAR_TABLE_NAME)
data class TravelCarEntity(
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo(name = "maker") val maker: String,
    @ColumnInfo(name = "model") val model: String,
    @ColumnInfo(name = "year") val year: Int,
    @ColumnInfo(name = "picture") val picture: String,
    @ColumnInfo(name = "image_data") val imageData: String?,
    @ColumnInfo(name = "equipments", defaultValue = "NULL") val equipments: List<String>? = null
) {
    companion object {
        const val TRAVEL_CAR_TABLE_NAME = "travel_car"
    }
}
