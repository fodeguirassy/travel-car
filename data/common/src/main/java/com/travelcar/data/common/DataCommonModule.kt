package com.travelcar.data.common

import android.content.Context
import androidx.room.Room
import com.travelcar.data.common.database.TRAVEL_CAR_DATABASE_NAME
import com.travelcar.data.common.database.TravelCarDatabase
import com.travelcar.data.common.database.travelcar.TravelCarDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.plugins.logging.LogLevel.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataCommonModule {

    @Singleton
    @Provides
    fun provideKtorHttpClient(): HttpClient = HttpClient {

        defaultRequest {
            header("Content-Type", "application/json")
        }

        install(ContentNegotiation) {
            json(
                Json {
                    isLenient = true
                    ignoreUnknownKeys = true
                    prettyPrint = true
                }
            )
        }
        install(Logging) {
            logger = Logger.DEFAULT
            level = ALL
        }

        expectSuccess = true
    }

    @Singleton
    @Provides
    fun provideTravelCarDatabase(
        @ApplicationContext context: Context
    ): TravelCarDatabase = Room
        .databaseBuilder(
            context,
            TravelCarDatabase::class.java,
            TRAVEL_CAR_DATABASE_NAME
        ).build()

    @Singleton
    @Provides
    fun provideTravelCarDao(
        travelCarDatabase: TravelCarDatabase
    ): TravelCarDao = travelCarDatabase.travelCarDao()
}
