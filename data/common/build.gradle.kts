plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    kotlin("kapt")
    id("com.google.dagger.hilt.android")
    kotlin("plugin.serialization")
}

android {
    compileSdk = Versions.AndroidConfig.compileSdk
    defaultConfig {
        minSdk = Versions.AndroidConfig.minSdk
    }
}

dependencies {
    api(Libraries.Hilt.hiltAndroid)
    kapt(Libraries.Hilt.hiltCompiler)

    api(Libraries.Ktor.ktorClientCore)
    api(Libraries.Ktor.ktorClientAndroid)
    api(Libraries.Ktor.ktorContentNegociation)
    api(Libraries.Ktor.ktorClientJson)
    api(Libraries.Ktor.ktorClientSerialization)
    api(Libraries.Ktor.ktorClientLogging)
    api(Libraries.KotLinExt.serializationJson)
    api(Libraries.Ktor.ktorSerializationCore)

    api(Libraries.Kotlin.stdLib)
    api(Libraries.Kotlin.reflect)

    api(Libraries.Room.runtime)
    kapt(Libraries.Room.compiler)
    api(Libraries.Room.extension)

    api(Libraries.gson)
}
