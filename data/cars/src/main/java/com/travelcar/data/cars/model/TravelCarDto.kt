package com.travelcar.data.cars.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TravelCarDto(
    @SerialName("equipments")
    val equipments: List<String>? = null,
    @SerialName("make")
    val maker: String,
    @SerialName("model")
    val model: String,
    @SerialName("picture")
    val picture: String,
    @SerialName("year")
    val year: Int
)