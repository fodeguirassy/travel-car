package com.travelcar.data.cars.repository

import com.travelcar.data.cars.network.TravelCarKtorClient
import com.travelcar.data.common.database.travelcar.TravelCarDao
import com.travelcar.data.common.database.travelcar.TravelCarEntity
import javax.inject.Inject

class CarsRepository @Inject constructor(
    private val client: TravelCarKtorClient,
    private val travelCarDao: TravelCarDao
) {

    suspend fun fetchCars() = client.fetchAllCars()

    suspend fun loadCars() = travelCarDao.getAllCars()

    fun observeCars() = travelCarDao.observeAllCars()

    suspend fun insertCars(cars: List<TravelCarEntity>) = travelCarDao.insertCars(cars)

    suspend fun fetchCarImageData(url: String) = client.fetchCarImageDate(url)
}
