package com.travelcar.data.cars.network

import com.travelcar.data.cars.model.TravelCarDto
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import java.io.InputStream
import javax.inject.Inject

class TravelCarKtorClient @Inject constructor(
    private val client: HttpClient
) {
    suspend fun fetchAllCars(): List<TravelCarDto> {
        return try {
            val res =
                client.get("https://gist.githubusercontent.com/ncltg/6a74a0143a8202a5597ef3451bde0d5a/raw/8fa93591ad4c3415c9e666f888e549fb8f945eb7/tc-test-ios.json")
            Json.decodeFromString(ListSerializer(TravelCarDto.serializer()), res.bodyAsText())
        } catch (cause: Throwable) {
            throw cause
        }
    }

    // TODO fix issues with travel car images raw data downloading
    // TODO working with following url https://images.squarespace-cdn.com/content/v1/60bd5838539722187fc9a157/1623995202395-7132W5DN573FH01B4DFZ/Spruceholme+Inn+-+Bistro+sample.png
    suspend fun fetchCarImageDate(url: String): InputStream = client.get(url).body()
}
