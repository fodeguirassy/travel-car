
plugins {
    id("com.android.application") version Versions.Plugins.androidApplication apply false
    id("com.android.library") version Versions.Plugins.androidLibrary apply false
    id("org.jetbrains.kotlin.android") version Versions.Kotlin.kotlin apply false
    id("com.google.dagger.hilt.android") version Versions.hilt apply false
    kotlin("plugin.serialization") version Versions.Kotlin.kotlin apply false
}

buildscript {
    dependencies {
        classpath(Libraries.Kotlin.serialization)
    }
}