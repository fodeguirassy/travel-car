package com.travelcar.feature.cardetails.ui

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.travelcar.feature.common.theme.TravelCarThemeDimensions

@Composable
fun CarDetailsSeparatorItem() {
    Spacer(
        modifier = Modifier
            .fillMaxWidth()
            .height(TravelCarThemeDimensions.m)
    )
}
