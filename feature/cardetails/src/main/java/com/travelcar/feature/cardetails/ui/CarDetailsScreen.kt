package com.travelcar.feature.cardetails.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideIn
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import com.travelcar.domain.cars.model.TravelCar
import com.travelcar.feature.cardetails.R
import com.travelcar.feature.cardetails.TravelCarDetailsActivity
import com.travelcar.feature.cars.ui.TravelCarImage
import com.travelcar.feature.common.theme.TravelCarTheme
import com.travelcar.feature.common.theme.TravelCarThemeDimensions
import com.travelcar.feature.common.toolbar.TravelCarToolbar

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CarDetailsScreen(
    car: TravelCar?
) {
    val context = LocalContext.current

    var animationVisible by remember { mutableStateOf(false) }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.surface)
    ) {
        if (car != null) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = TravelCarThemeDimensions.s)
            ) {
                stickyHeader {
                    TravelCarToolbar(
                        isBackButtonVisible = true
                    ) {
                        (context as? TravelCarDetailsActivity)?.finish()
                    }
                }
                item {
                    AnimatedVisibility(
                        visible = animationVisible,
                        enter = slideIn(
                            tween(5000, easing = LinearOutSlowInEasing)
                        ) { fullSize ->
                            IntOffset(fullSize.width - 500, 0)
                        }
                    ) {
                        TravelCarImage(
                            url = car.picture
                        )
                    }
                }

                item {
                    CarDetailsSeparatorItem()
                }

                item {
                    TravelCarDetailsInfo(car = car)
                }

                item {
                    CarDetailsSeparatorItem()
                }

                TravelCarEquipment.values().map {
                    item {
                        CarDetailsEquipmentItem(
                            equipmentName = it.label,
                            isIncluded = car.equipments?.contains(it.label) ?: false
                        )
                    }
                }

                car.equipments?.filter { it !in TravelCarEquipment.values().map { it.label } }
                    ?.map {
                        item {
                            CarDetailsEquipmentItem(
                                equipmentName = it,
                                isIncluded = true
                            )
                        }
                    }

                item {
                    CarDetailsSeparatorItem()
                }
            }
        } else {
            Text(
                text = stringResource(id = R.string.car_details_error),
                style = MaterialTheme.typography.subtitle1
            )
        }
    }

    LaunchedEffect(key1 = Unit) {
        animationVisible = !animationVisible
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun CarDetailsScreenPreview() {
    TravelCarTheme {
        CarDetailsScreen(
            car = TravelCar(
                equipments = listOf("test", "test", "test"),
                maker = "Renault",
                model = "clio",
                year = 2009,
                picture = "https://www.travelcar.com/api/media/20170824/dJs3itYPcL1PBPLtTXElInJiK0cGN0SPCDsyyJBOYDjlnLMyzXgrIxRm3c10D30MxvUyf7akpF5Nj60jls5OUINCgbNmWEnxOXWWzfds7JtNDaQOjvf5ntKw5Jt15h2K/twingo-b.png"
            )
        )
    }
}
