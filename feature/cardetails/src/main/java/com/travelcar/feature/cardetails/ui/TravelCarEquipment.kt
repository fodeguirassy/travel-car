package com.travelcar.feature.cardetails.ui

enum class TravelCarEquipment(
    val label: String
) {
    GPS("GPS"),
    BABY_SEAT("Siège enfant"),
    SUPPORT("Assistance 24h/24"),
    AC("Climatisation"),
    ABS("ABS"),
    AIRBAGS("Airbags");
}