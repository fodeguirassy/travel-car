package com.travelcar.feature.cardetails.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.travelcar.domain.cars.model.TravelCar
import com.travelcar.feature.cardetails.R.string
import com.travelcar.feature.common.theme.TravelCarThemeDimensions

@Composable
fun TravelCarDetailsInfo(
    car: TravelCar
) {
    Surface(
        modifier = Modifier.fillMaxWidth()
            .padding(horizontal = TravelCarThemeDimensions.s),
        elevation = TravelCarThemeDimensions.xxxs,
        shape = RoundedCornerShape(TravelCarThemeDimensions.xxs)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = TravelCarThemeDimensions.s),
            verticalArrangement = Arrangement.spacedBy(TravelCarThemeDimensions.xs)
        ) {
            Text(
                text = stringResource(id = string.card_detail_info_prefix_maker, car.maker),
                style = MaterialTheme.typography.body1
            )
            Text(
                text = stringResource(id = string.card_detail_info_prefix_model, car.model),
                style = MaterialTheme.typography.body1
            )
            Text(
                text = stringResource(id = string.card_detail_info_prefix_year, car.year),
                style = MaterialTheme.typography.body1
            )
        }
    }
}