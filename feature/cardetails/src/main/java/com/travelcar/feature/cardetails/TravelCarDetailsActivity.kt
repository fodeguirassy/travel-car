package com.travelcar.feature.cardetails

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.travelcar.domain.cars.model.TravelCar
import com.travelcar.feature.cardetails.ui.CarDetailsScreen
import com.travelcar.feature.common.navigation.TRAVEL_CAR_PARAM_KEY
import com.travelcar.feature.common.theme.TravelCarTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TravelCarDetailsActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val carParams = intent.getParcelableExtra<TravelCar>(TRAVEL_CAR_PARAM_KEY)

        setContent {
            TravelCarTheme {
                val systemUiController = rememberSystemUiController()
                SideEffect { systemUiController.setStatusBarColor(Color.White, true) }
                CarDetailsScreen(car = carParams)
            }
        }
    }
}
