package com.travelcar.feature.cardetails.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.travelcar.feature.cardetails.R
import com.travelcar.feature.common.theme.TravelCarTheme
import com.travelcar.feature.common.theme.TravelCarThemeDimensions

@Composable
fun CarDetailsEquipmentItem(
    equipmentName: String,
    isIncluded: Boolean
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.surface)
            .padding(horizontal = TravelCarThemeDimensions.m, vertical = TravelCarThemeDimensions.xxs),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {

        Text(
            text = equipmentName,
            style = MaterialTheme.typography.body1
        )

        AnimatedVisibility(
            visible = true
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_check.takeIf { isIncluded } ?: R.drawable.ic_clear),
                contentDescription = null,
                tint = Color.Green.takeIf { isIncluded } ?: Color.Red
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CarDetailsEquipmentItemPreview() {
    TravelCarTheme {
        CarDetailsEquipmentItem(
            "Assistance 24h/24",
            isIncluded = true
        )
    }
}
