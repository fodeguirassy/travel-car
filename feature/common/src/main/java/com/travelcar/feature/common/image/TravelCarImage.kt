package com.travelcar.feature.cars.ui

import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import coil.compose.AsyncImage
import coil.request.ImageRequest.Builder
import com.travelcar.feature.common.R

@Composable
fun TravelCarImage(
    url: String
) {
    AsyncImage(
        model = Builder(LocalContext.current)
            .data(url)
            .crossfade(true)
            .build(),
        contentDescription = null,
        modifier = Modifier
            .fillMaxWidth()
            .aspectRatio(16f / 9f),
        placeholder = painterResource(id = R.drawable.car_default),
        contentScale = ContentScale.Fit
    )
}
