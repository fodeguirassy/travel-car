package com.travelcar.feature.common.theme

import androidx.compose.ui.unit.dp

object TravelCarThemeDimensions {
    val xxxs = 2.dp
    val xxs = 4.dp
    val xs = 8.dp
    val s = 12.dp
    val m = 16.dp
    val l = 24.dp
    val xl = 32.dp
    val xxl = 48.dp
    val xxxl = 54.dp
}
