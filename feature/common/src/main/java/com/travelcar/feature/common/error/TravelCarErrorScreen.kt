package com.travelcar.feature.common.error

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.travelcar.feature.common.R
import com.travelcar.feature.common.theme.TravelCarThemeDimensions

@Composable
fun TravelCarErrorScreen() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.surface),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(id = R.string.app_error_text),
            style = MaterialTheme.typography.subtitle1,
            modifier = Modifier
                .padding(horizontal = TravelCarThemeDimensions.xxxl),
            textAlign = TextAlign.Center
        )
    }
}
