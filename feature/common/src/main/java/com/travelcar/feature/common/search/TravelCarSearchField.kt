package com.travelcar.feature.common.search

import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import com.travelcar.feature.common.theme.TravelCarThemeDimensions

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TravelCarSearchField(
    value: String,
    onValueChanged: (String) -> Unit,
    @StringRes placeholderResId: Int
) {
    var inputValue by remember { mutableStateOf("") }

    val keyboardController = LocalSoftwareKeyboardController.current

    val placeholderText = stringResource(id = placeholderResId)

    val focusManager = LocalFocusManager.current

    var hasUserFocus by remember { mutableStateOf(false) }

    Box(
        modifier = Modifier.fillMaxWidth()
            .background(color = MaterialTheme.colors.surface)
            .padding(vertical = TravelCarThemeDimensions.s, horizontal = TravelCarThemeDimensions.l)
            .clickable {
                hasUserFocus = !hasUserFocus
            }
    ) {
        TextField(
            modifier = Modifier
                .fillMaxWidth(),
            value = inputValue,
            onValueChange = {
                inputValue = it
                onValueChanged(inputValue)
            },
            placeholder = {
                Text(
                    text = placeholderText,
                    style = MaterialTheme.typography.caption
                )
            },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions(
                onSearch = {
                    keyboardController?.hide()
                    focusManager.clearFocus(force = true)
                }
            ),
            trailingIcon = {
                Surface(
                    shape = CircleShape,
                    elevation = TravelCarThemeDimensions.xs,
                    color = MaterialTheme.colors.surface,
                    contentColor = MaterialTheme.colors.primaryVariant,
                    modifier = Modifier
                        .padding(all = TravelCarThemeDimensions.xs)
                        .size(TravelCarThemeDimensions.l)
                ) {
                    IconButton(
                        onClick = {
                            inputValue = ""
                            onValueChanged("")
                            keyboardController?.hide()
                            focusManager.clearFocus(force = true)
                        },
                        modifier = Modifier
                            .padding(all = TravelCarThemeDimensions.xxs)
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Clear,
                            contentDescription = null
                        )
                    }
                }
            },
            shape = RoundedCornerShape(TravelCarThemeDimensions.s),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = MaterialTheme.colors.surface,
                cursorColor = MaterialTheme.colors.primaryVariant
            )
        )
    }
}
