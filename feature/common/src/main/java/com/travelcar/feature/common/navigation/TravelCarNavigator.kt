package com.travelcar.feature.common.navigation

import android.content.Context
import com.travelcar.domain.cars.model.TravelCar

interface TravelCarNavigator {
    fun toTravelCarDetails(
        car: TravelCar,
        context: Context
    )
}