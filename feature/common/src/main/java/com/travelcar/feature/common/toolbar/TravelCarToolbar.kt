package com.travelcar.feature.common.toolbar

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.travelcar.feature.common.R.string
import com.travelcar.feature.common.theme.TravelCarThemeDimensions

@Composable
fun TravelCarToolbar(
    isBackButtonVisible: Boolean = false,
    onBackClicked: () -> Unit = {}
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colors.surface)
            .padding(bottom = TravelCarThemeDimensions.l, top = TravelCarThemeDimensions.m)
    ) {
        if (isBackButtonVisible) {
            Image(
                imageVector = Icons.Filled.ArrowBack,
                contentDescription = null,
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .clickable {
                        onBackClicked()
                    }
            )
        }

        Text(
            text = stringResource(id = string.app_name),
            style = MaterialTheme.typography.h1,
            modifier = Modifier
                .align(Alignment.Center)
        )
    }
}
