package com.travelcar.feature.common.navigation

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.travelcar.feature.common.R

enum class TravelCarDestination(
    val route: String,
    @StringRes val titleResId: Int,
    @DrawableRes val iconResId: Int
) {
    CARS(
        route = "cars",
        titleResId = R.string.navigation_cars_title,
        iconResId = R.drawable.ic_car
    ),
    PROFILE(
        route = "profile",
        titleResId = R.string.navigation_profile_title,
        iconResId = R.drawable.ic_person
    )
}
