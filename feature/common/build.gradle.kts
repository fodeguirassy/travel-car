apply(from = "../../buildSrc/feature-common/gradle.kts")

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("com.google.dagger.hilt.android")
    id("kotlin-parcelize")
    kotlin("kapt")
}

dependencies {
    api(Libraries.Hilt.hiltAndroid)
    kapt(Libraries.Hilt.hiltCompiler)

    api(Libraries.Androidx.kotlinExtCore)
    api(Libraries.Androidx.lifecycle)
    api(Libraries.Androidx.lifecycleViewModel)
    api(Libraries.KotLinExt.coroutine)

    // Compose
    api(Libraries.Compose.activityCompose)
    api(Libraries.Compose.composeUi)
    api(Libraries.Compose.composePreviewTooling)
    api(Libraries.Compose.composeMaterial)
    api(Libraries.Compose.navigationCompose)
    api(Libraries.Compose.hiltComposeNavigation)
    api(Libraries.Compose.accompanist)

    api(Libraries.coil)

    api(project(":domain:cars"))
}