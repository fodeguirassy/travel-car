package com.travelcar.feature.cars.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import com.travelcar.domain.cars.model.TravelCarResult.Loading
import com.travelcar.domain.cars.model.TravelCarResult.Success
import com.travelcar.feature.cars.CarsViewModel
import com.travelcar.feature.common.error.TravelCarErrorScreen
import com.travelcar.feature.common.loader.TravelCarLoader
import com.travelcar.feature.common.navigation.TravelCarNavigator
import com.travelcar.feature.common.search.TravelCarSearchField
import com.travelcar.feature.common.theme.TravelCarThemeDimensions
import com.travelcar.feature.common.toolbar.TravelCarToolbar

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CarsScreen(
    travelCarNavigator: TravelCarNavigator,
    carsViewModel: CarsViewModel = hiltViewModel(),
) {
    val carsState = carsViewModel.allCarsState.collectAsState(Loading).value
    val queryState = carsViewModel.searchQuery.collectAsState()

    val context = LocalContext.current

    val isLoading = carsViewModel.isLoading.collectAsState(initial = true).value
    val stateFailed = carsViewModel.stateFailed.collectAsState(initial = true).value

    Box(
        modifier = Modifier
            .fillMaxSize(),
    ) {
        if (carsState is Success) {
            AnimatedVisibility(
                visible = true,
                Modifier
                    .fillMaxSize(),
            ) {
                LazyColumn(
                    Modifier
                        .fillMaxSize(),
                ) {
                    stickyHeader {
                        TravelCarToolbar()
                        TravelCarSearchField(
                            value = queryState.value,
                            onValueChanged = {
                                carsViewModel.setSearchQuery(it)
                            },
                            placeholderResId = com.travelcar.feature.cars.R.string.travel_car_search_placeholder,
                        )
                    }

                    carsState.data?.mapIndexed { index, item ->
                        item(
                            key = item.picture,
                        ) {
                            CarItemScreen(
                                car = item,
                                query = queryState.value,
                                modifier = Modifier.animateItemPlacement(),
                            ) {
                                travelCarNavigator.toTravelCarDetails(
                                    car = item,
                                    context = context,
                                )
                            }
                            if (index == carsState.data?.lastIndex) {
                                Spacer(modifier = Modifier.height(TravelCarThemeDimensions.xxxl))
                            }
                        }
                    }
                }
            }
        }

        AnimatedVisibility(
            visible = stateFailed,
            modifier = Modifier
                .fillMaxSize(),
        ) {
            TravelCarErrorScreen()
        }

        AnimatedVisibility(
            visible = isLoading,
            modifier = Modifier
                .fillMaxSize(),
        ) {
            TravelCarLoader()
        }
    }
}
