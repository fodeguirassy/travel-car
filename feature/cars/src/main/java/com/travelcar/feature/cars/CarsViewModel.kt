package com.travelcar.feature.cars

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.travelcar.domain.cars.model.TravelCar
import com.travelcar.domain.cars.model.TravelCarResult
import com.travelcar.domain.cars.model.TravelCarResult.Error
import com.travelcar.domain.cars.model.TravelCarResult.Loading
import com.travelcar.domain.cars.model.TravelCarResult.Success
import com.travelcar.domain.cars.model.toRomanCharactersRemoved
import com.travelcar.domain.cars.usecase.ObserveCars
import com.travelcar.domain.cars.usecase.SyncCars
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CarsViewModel @Inject constructor(
    private val syncCars: SyncCars,
    private val observeCars: ObserveCars,
) : ViewModel() {

    private val _syncCarsResult = MutableStateFlow<TravelCarResult<Any>>(TravelCarResult.Loading)
    private val _allCarsState = MutableStateFlow<TravelCarResult<List<TravelCar>>>(TravelCarResult.Loading)

    private val _searchQuery = MutableStateFlow("")
    val searchQuery = _searchQuery.asStateFlow()

    val allCarsState = combine(
        _allCarsState,
        _searchQuery,
    ) { carsState, query ->

        return@combine when {
            query.isNotBlank() && carsState is Success -> {
                val data = carsState.data ?: return@combine carsState
                val newData = data.sortedByDescending {
                    val modelName = it.model.toRomanCharactersRemoved()
                    val lowercaseQuery = query.lowercase()
                    modelName.count { it in lowercaseQuery }
                }

                return@combine Success(newData)
            }
            else -> carsState
        }
    }

    val isLoading = combine(
        _syncCarsResult,
        _allCarsState,
    ) { syncState, carsState ->
        return@combine syncState is Loading || carsState is Loading
    }

    val stateFailed = combine(
        _syncCarsResult,
        _allCarsState,
    ) { syncState, carsState ->
        return@combine syncState is Error || carsState is Error
    }

    fun initData() {
        viewModelScope.launch {
            observeCars()
                .flowOn(Dispatchers.IO)
                .catch {
                    _allCarsState.value = TravelCarResult.Error()
                }
                .collect {
                    it?.let {
                        _allCarsState.value = Success(it)
                    }
                }
        }

        viewModelScope.launch {
            _syncCarsResult.value = syncCars.invoke()
        }
    }

    fun setSearchQuery(newQuery: String) {
        _searchQuery.value = newQuery
    }
}
