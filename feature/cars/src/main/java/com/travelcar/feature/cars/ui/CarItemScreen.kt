package com.travelcar.feature.cars.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import com.travelcar.domain.cars.model.TravelCar
import com.travelcar.feature.cars.R
import com.travelcar.feature.common.theme.TravelCarTheme
import com.travelcar.feature.common.theme.TravelCarThemeDimensions

@Composable
fun CarItemScreen(
    modifier: Modifier,
    car: TravelCar,
    query: String,
    onCarClick: (TravelCar) -> Unit
) {

    val context = LocalContext.current

    Surface(
        modifier = modifier
            .fillMaxWidth()
            .padding(
                start = TravelCarThemeDimensions.m,
                end = TravelCarThemeDimensions.m,
                top = TravelCarThemeDimensions.m,
                bottom = TravelCarThemeDimensions.s
            )
            .clickable {
                onCarClick(car)
            },
        elevation = TravelCarThemeDimensions.xs,
        shape = RoundedCornerShape(TravelCarThemeDimensions.m)
    ) {
        Box(
            modifier = Modifier.fillMaxWidth()
                .padding(bottom = TravelCarThemeDimensions.m)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                TravelCarImage(
                    url = car.picture
                )
                Spacer(modifier = Modifier.height(TravelCarThemeDimensions.xs))
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = TravelCarThemeDimensions.m),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {

                    Text(
                        text = buildAnnotatedString {
                            car.model.map { char ->
                                val charToString = "$char"
                                this.append(charToString)

                                if (char.lowercase() in query.lowercase()) {
                                    val start = car.model.indexOfAny(listOf(charToString), ignoreCase = true)
                                    val end = start + 1
                                    val style = SpanStyle(color = MaterialTheme.colors.secondary)
                                    this.addStyle(style, start, end)
                                }
                            }
                            this.toAnnotatedString()
                        },
                        style = MaterialTheme.typography.h1,
                        modifier = Modifier
                    )
                    Text(
                        text = stringResource(id = R.string.car_item_year, formatArgs = arrayOf(car.year)),
                        style = MaterialTheme.typography.subtitle1
                    )
                }

                val equipmentsCount = car.equipments?.count() ?: 0
                Text(
                    text = context.resources.getQuantityString(
                        R.plurals.car_item_equipments_count,
                        equipmentsCount,
                        equipmentsCount
                    ),
                    style = MaterialTheme.typography.body1,
                    modifier = Modifier
                        .padding(start = TravelCarThemeDimensions.m)
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CarItemScreenPreview() {
    TravelCarTheme {
        CarItemScreen(
            modifier = Modifier,
            car = TravelCar(
                equipments = listOf("test", "test", "test"),
                maker = "Renault",
                model = "clio",
                year = 2009,
                picture = "https://www.travelcar.com/api/media/20170824/dJs3itYPcL1PBPLtTXElInJiK0cGN0SPCDsyyJBOYDjlnLMyzXgrIxRm3c10D30MxvUyf7akpF5Nj60jls5OUINCgbNmWEnxOXWWzfds7JtNDaQOjvf5ntKw5Jt15h2K/twingo-b.png"
            ),
            query = ""
        ) {
        }
    }
}
