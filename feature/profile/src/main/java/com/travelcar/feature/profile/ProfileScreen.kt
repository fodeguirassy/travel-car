package com.travelcar.feature.profile

import androidx.compose.material.Text
import androidx.compose.runtime.Composable

@Composable
fun ProfileScreen() {
    Text(
        text = "Hello Profile Screen"
    )
}
