apply(from = "../../buildSrc/feature-common/gradle.kts")

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("com.google.dagger.hilt.android")
    kotlin("kapt")
}

dependencies {
    api(Libraries.Hilt.hiltAndroid)
    kapt(Libraries.Hilt.hiltCompiler)
    implementation(project(":feature:common"))
    implementation(project(":domain:cars"))
}