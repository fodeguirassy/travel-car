import Libraries.Hilt

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.dagger.hilt.android")
    id("kotlin-parcelize")
    kotlin("kapt")
    kotlin("plugin.serialization")
}

android {
    namespace = "com.test.travelcar"
    compileSdk = Versions.AndroidConfig.compileSdk

    defaultConfig {
        applicationId = Versions.AndroidConfig.applicationId
        minSdk = Versions.AndroidConfig.minSdk
        targetSdk = Versions.AndroidConfig.targetSdk
        versionCode = Versions.AndroidConfig.versionCode
        versionName = Versions.AndroidConfig.versionName
        compileSdk = Versions.AndroidConfig.compileSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.1.1"
    }
}

dependencies {

    api(Hilt.hiltAndroid)
    kapt(Hilt.hiltCompiler)

    debugImplementation(Libraries.Compose.composeTooling)

    implementation(project(":feature:cars"))
    implementation(project(":feature:common"))
    implementation(project(":feature:profile"))
    implementation(project(":feature:cardetails"))

    testImplementation(Libraries.Test.jUnit)
    testImplementation(Libraries.Test.mockk)
    testImplementation(Libraries.Test.coroutine)
    testImplementation(Libraries.Test.turbine)
}

hilt {
    enableExperimentalClasspathAggregation = true
    enableAggregatingTask = true
}
