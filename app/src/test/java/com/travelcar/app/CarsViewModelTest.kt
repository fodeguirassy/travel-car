package com.travelcar.app

import com.travelcar.app.common.MainCoroutineRule
import com.travelcar.data.cars.repository.CarsRepository
import com.travelcar.domain.cars.usecase.ObserveCars
import com.travelcar.domain.cars.usecase.SyncCars
import com.travelcar.feature.cars.CarsViewModel
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CarsViewModelTest {

    private lateinit var carsRepository: CarsRepository
    private lateinit var carsViewModel: CarsViewModel
    private lateinit var syncCars: SyncCars
    private lateinit var observeCars: ObserveCars

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val coroutineRule = MainCoroutineRule()

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        MockKAnnotations.init(this)
        carsRepository = CarsRepository(mockk(relaxed = true), mockk(relaxed = true))
        syncCars = SyncCars(
            carsRepository = carsRepository,
            UnconfinedTestDispatcher()
        )

        observeCars = ObserveCars(
            carsRepository = carsRepository,
            coroutineDispatcher = UnconfinedTestDispatcher()
        )

        carsViewModel = CarsViewModel(
            syncCars = syncCars,
            observeCars = observeCars
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `should call repository fetch cars as database is empty`() = runTest {
        coEvery { carsRepository.loadCars() } returns emptyList()
        carsViewModel.initData()
        coVerify { carsRepository.fetchCars() }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `should not call repository fetch cars as database is not empty`() = runTest {
        coEvery { carsRepository.loadCars() } returns listOf(mockk())
        carsViewModel.initData()
        coVerify(exactly = 0) { carsRepository.fetchCars() }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}

