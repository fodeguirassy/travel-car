package com.travelcar.app

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.travelcar.feature.cars.CarsViewModel
import com.travelcar.feature.cars.ui.CarsScreen
import com.travelcar.feature.common.navigation.TravelCarDestination
import com.travelcar.feature.common.navigation.TravelCarNavigator
import com.travelcar.feature.common.theme.TravelCarTheme
import com.travelcar.feature.profile.ProfileScreen
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel by viewModels<CarsViewModel>()

    @Inject
    lateinit var travelCarNavigator: TravelCarNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initData()
        setContent {
            val systemUiController = rememberSystemUiController()
            SideEffect { systemUiController.setStatusBarColor(Color.White, true) }
            TravelCarTheme {
                MainScreen(
                    travelCarNavigator = travelCarNavigator
                )
            }
        }
    }
}

@Composable
fun MainScreen(
    travelCarNavigator: TravelCarNavigator,
    carsViewModel: CarsViewModel = hiltViewModel()
) {
    val navigationController = rememberNavController()
    val navigationItems = TravelCarDestination.values()
    Scaffold(
        bottomBar = {
            BottomNavigation(
                backgroundColor = MaterialTheme.colors.surface
            ) {
                val navBackStackEntry by navigationController.currentBackStackEntryAsState()
                val currentDestination = navBackStackEntry?.destination
                navigationItems.forEach { travelCarDestination ->
                    BottomNavigationItem(
                        selected = currentDestination?.hierarchy?.any { it.route == travelCarDestination.route } == true,
                        onClick = {
                            navigationController.navigate(travelCarDestination.route) {
                                popUpTo(navigationController.graph.findStartDestination().id) {
                                    saveState = true
                                }
                                launchSingleTop = true
                                restoreState = true
                            }
                        },
                        icon = {
                            Icon(
                                painter = painterResource(id = travelCarDestination.iconResId),
                                contentDescription = null
                            )
                        },
                        label = {
                            Text(
                                text = stringResource(id = travelCarDestination.titleResId),
                                style = MaterialTheme.typography.button
                            )
                        },
                        selectedContentColor = MaterialTheme.colors.primaryVariant,
                        unselectedContentColor = MaterialTheme.colors.onSurface
                    )
                }
            }
        }
    ) {
        NavHost(
            navController = navigationController,
            startDestination = TravelCarDestination.CARS.route
        ) {
            composable(TravelCarDestination.CARS.route) {
                CarsScreen(
                    carsViewModel = carsViewModel,
                    travelCarNavigator = travelCarNavigator
                )
            }
            composable(TravelCarDestination.PROFILE.route) { ProfileScreen() }
        }
    }
}
