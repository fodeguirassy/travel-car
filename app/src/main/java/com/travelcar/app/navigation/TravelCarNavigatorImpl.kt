package com.travelcar.app.navigation

import android.content.Context
import android.content.Intent
import com.travelcar.domain.cars.model.TravelCar
import com.travelcar.feature.cardetails.TravelCarDetailsActivity
import com.travelcar.feature.common.navigation.TRAVEL_CAR_PARAM_KEY
import com.travelcar.feature.common.navigation.TravelCarNavigator
import javax.inject.Inject

class TravelCarNavigatorImpl @Inject constructor(): TravelCarNavigator {
    override fun toTravelCarDetails(
        car: TravelCar,
        context: Context
    ) {
        context.startActivity(
            Intent(context, TravelCarDetailsActivity::class.java).apply {
                putExtra(TRAVEL_CAR_PARAM_KEY, car)
            }
        )
    }
}
