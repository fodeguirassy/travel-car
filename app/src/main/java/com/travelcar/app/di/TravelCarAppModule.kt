package com.travelcar.app.di

import com.travelcar.app.navigation.TravelCarNavigatorImpl
import com.travelcar.feature.common.navigation.TravelCarNavigator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class TravelCarAppModule {

    @Singleton
    @Provides
    fun provideTravelCarNavigator(implementation: TravelCarNavigatorImpl): TravelCarNavigator = implementation
}
