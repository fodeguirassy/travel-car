object Libraries {

    object Compose {
        const val activityCompose = "androidx.activity:activity-compose:${Versions.Compose.activityCompose}"
        const val composeUi = "androidx.compose.ui:ui:${Versions.Compose.ui}"
        const val composePreviewTooling = "androidx.compose.ui:ui-tooling-preview:${Versions.Compose.ui}"
        const val composeTooling = "androidx.compose.ui:ui-tooling:${Versions.Compose.ui}"
        const val composeMaterial = "androidx.compose.material:material:${Versions.Compose.ui}"
        const val navigationCompose = "androidx.navigation:navigation-compose:${Versions.Compose.navigation}"
        const val hiltComposeNavigation = "androidx.hilt:hilt-navigation-compose:${Versions.Compose.hiltNavigation}"
        const val accompanist = "com.google.accompanist:accompanist-systemuicontroller:${Versions.Compose.accompanist}"
    }

    object Androidx {
        const val kotlinExtCore = "androidx.core:core-ktx:${Versions.KotlinExt.core}"
        const val lifecycle = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.KotlinExt.lifecycle}"
        const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.KotlinExt.lifecycle}"
    }

    object Test {
        const val jUnit = "junit:junit:${Versions.Test.jUnit}"
        const val mockk = "io.mockk:mockk:${Versions.Test.mockk}"
        const val mockkAndroid = "io.mockk:mockk-android:${Versions.Test.mockk}"
        const val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.Test.coroutine}"
        const val turbine = "app.cash.turbine:turbine:${Versions.Test.turbine}"
    }

    object Hilt {
        const val hiltAndroid = "com.google.dagger:hilt-android:${Versions.hilt}"
        const val hiltCompiler = "com.google.dagger:hilt-android-compiler:${Versions.hilt}"
    }

    object Ktor {
        const val ktorClientCore = "io.ktor:ktor-client-core:${Versions.Ktor.ktorClient}"
        const val ktorClientAndroid = "io.ktor:ktor-client-android:${Versions.Ktor.ktorClient}"
        const val ktorContentNegociation = "io.ktor:ktor-client-content-negotiation:${Versions.Ktor.ktorClient}"
        const val ktorClientJson = "io.ktor:ktor-client-json:${Versions.Ktor.ktorClient}"
        const val ktorClientSerialization = "io.ktor:ktor-serialization-kotlinx-json:${Versions.Ktor.ktorClient}"
        const val ktorClientLogging = "io.ktor:ktor-client-logging:${Versions.Ktor.ktorClient}"
        const val ktorSerializationCore = "io.ktor:ktor-serialization:${Versions.Ktor.ktorClient}"
    }

    object KotLinExt {
        const val serializationJson =
            "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.KotlinExt.serialization}"
        const val coroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.KotlinExt.coroutines}"
    }

    object Kotlin {
        const val serialization = "org.jetbrains.kotlin:kotlin-serialization:${Versions.Kotlin.kotlin}"
        const val stdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.Kotlin.kotlin}"
        const val reflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.Kotlin.kotlin}"
    }

    object Room {
        const val runtime = "androidx.room:room-runtime:${Versions.room}"
        const val compiler = "androidx.room:room-compiler:${Versions.room}"
        const val extension = "androidx.room:room-ktx:${Versions.room}"
    }

    const val gson = "com.google.code.gson:gson:${Versions.gson}"

    const val coil = "io.coil-kt:coil-compose:${Versions.coil}"
}
