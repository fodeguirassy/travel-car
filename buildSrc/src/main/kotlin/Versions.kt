object Versions {
    object Compose {
        const val ui = "1.1.1"
        const val activityCompose = "1.3.1"
        const val navigation = "2.5.3"
        const val hiltNavigation = "1.0.0"
        const val accompanist = "0.28.0"
    }

    object Plugins {
        const val androidApplication = "7.3.1"
        const val androidLibrary = "7.3.1"
    }

    object Kotlin {
        const val kotlin = "1.6.10"
    }

    object AndroidConfig {
        const val applicationId = "com.travelcar"
        const val minSdk = 21
        const val targetSdk = 32
        const val versionCode = 1
        const val versionName = "1.0"
        const val compileSdk = 33
    }

    object KotlinExt {
        const val core = "1.7.0"
        const val lifecycle = "2.4.0"
        const val serialization = "1.0.1"
        const val coroutines = "1.6.4"
    }

    const val hilt = "2.44.2"

    object Test {
        const val jUnit = "4.13.2"
        const val mockk = "1.13.3"
        const val coroutine = "1.6.4"
        const val turbine = "0.12.1"
    }

    object Ktor {
        const val ktorClient = "2.2.2"
    }

    const val room = "2.4.3"

    const val gson = "2.10.1"

    const val coil = "2.2.2"
}
