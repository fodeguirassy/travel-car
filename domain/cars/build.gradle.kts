plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("com.google.dagger.hilt.android")
    id("kotlin-parcelize")
    kotlin("kapt")
}

android {
    compileSdk = Versions.AndroidConfig.compileSdk

    defaultConfig {
        minSdk = Versions.AndroidConfig.minSdk
    }
}

dependencies {
    api(Libraries.Hilt.hiltAndroid)
    kapt(Libraries.Hilt.hiltCompiler)

    implementation(project(":domain:common"))
    api(project(":data:cars"))
}

hilt {
    enableExperimentalClasspathAggregation = true
    enableAggregatingTask = true
}
