package com.travelcar.domain.cars.model

object CarsRomanCharacters {

    val characters = listOf(
        "I",
        "V",
    )
}
