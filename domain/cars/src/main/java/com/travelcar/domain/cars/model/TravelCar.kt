package com.travelcar.domain.cars.model

import android.os.Parcelable
import com.travelcar.data.cars.model.TravelCarDto
import com.travelcar.data.common.database.travelcar.TravelCarEntity
import kotlinx.parcelize.Parcelize

@Parcelize
data class TravelCar(
    val equipments: List<String>? = null,
    val maker: String,
    val model: String,
    val picture: String,
    val year: Int,
    var imageData: String? = null,
) : Parcelable {
    companion object {
        fun fromEntity(entity: TravelCarEntity) = TravelCar(
            equipments = entity.equipments,
            maker = entity.maker,
            model = entity.model,
            picture = entity.picture,
            year = entity.year,
            imageData = entity.imageData,
        )

        fun fromDto(dto: TravelCarDto) = TravelCar(
            equipments = dto.equipments,
            maker = dto.maker,
            model = dto.model,
            picture = dto.picture,
            year = dto.year,
        )
    }
}

internal fun TravelCar.toEntity() = TravelCarEntity(
    equipments = equipments,
    maker = maker,
    model = model,
    picture = picture,
    year = year,
    imageData = imageData,
)

fun String.toRomanCharactersRemoved(): String {
    val split = this.split(" ").map { it.lowercase() }
    val removed = split.filter { it.first().toString() !in CarsRomanCharacters.characters.map { it.lowercase() } }
    return removed.joinToString(" ").trim()
}
