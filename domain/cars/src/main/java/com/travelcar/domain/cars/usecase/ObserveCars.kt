package com.travelcar.domain.cars.usecase

import com.travelcar.data.cars.repository.CarsRepository
import com.travelcar.domain.cars.model.TravelCar
import com.travelcar.domain.common.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ObserveCars @Inject constructor(
    private val carsRepository: CarsRepository,
    @IoDispatcher val coroutineDispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    operator fun invoke(): Flow<List<TravelCar>?> {
        return carsRepository.observeCars().map { it?.map { TravelCar.fromEntity(it) } }
    }
}
