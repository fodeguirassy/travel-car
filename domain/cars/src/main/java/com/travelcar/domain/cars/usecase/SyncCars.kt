package com.travelcar.domain.cars.usecase

import com.travelcar.data.cars.repository.CarsRepository
import com.travelcar.domain.cars.model.TravelCar
import com.travelcar.domain.cars.model.TravelCarResult
import com.travelcar.domain.cars.model.toEntity
import com.travelcar.domain.common.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SyncCars @Inject constructor(
    private val carsRepository: CarsRepository,
    @IoDispatcher private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend operator fun invoke(): TravelCarResult<Any> {
        return try {
            withContext(dispatcher) {
                val localCars = carsRepository.loadCars()
                if (localCars.isNullOrEmpty()) {
                    val cars = carsRepository.fetchCars().map {
                        val domainModel = TravelCar.fromDto(it)
                        val entityModel = domainModel.toEntity()
                        entityModel
                    }
                    carsRepository.insertCars(cars)
                }
                TravelCarResult.Success(Any())
            }
        } catch (cause: Throwable) {
            TravelCarResult.Error(cause)
        }
    }
}
