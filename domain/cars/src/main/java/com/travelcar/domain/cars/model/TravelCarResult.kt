package com.travelcar.domain.cars.model

sealed class TravelCarResult<out T> {
    object Loading : TravelCarResult<Nothing>()
    data class Error(
        val cause: Throwable? = null
    ) : TravelCarResult<Nothing>()
    data class Success<out R>(val data: R?) : TravelCarResult<R>()
}
