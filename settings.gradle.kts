pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
        maven("kotlin.bintray.com/kotlinx")
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "travelcar"
include(":app")
include(":data:cars")
include(":data:common")
include(":domain:cars")
include(":domain:common")
include(":feature:cars")
include(":feature:common")
include(":feature:profile")
include(":feature:cardetails")
